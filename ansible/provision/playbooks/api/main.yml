---
- hosts: localhost
  connection: local
  name: API
  gather_facts: false

  vars_files:
  - ../vars.yml
  - vars.yml

  tasks:
    - name: Create public IP
      azure_rm_publicipaddress:
        resource_group: "{{ resource_group_name }}"
        sku: Standard
        allocation_method: Static
        name: "{{ public_ip_name }}"
      register: output_ip_address

    - name: Create an "A" record to public IP address
      azure_rm_dnsrecordset:
        resource_group: "{{ zone_rg_name }}"
        relative_name: "{{ sub_domain }}"
        zone_name: "{{ zone_name }}"
        record_type: A
        records:
          - entry: "{{ output_ip_address.state.ip_address }}"

    - name: Create security group
      azure_rm_securitygroup:
        resource_group: "{{ resource_group_name }}"
        name: "{{ security_group_name }}"
        rules: "{{ security_group_rules }}"

    - name: Create load balancer
      azure_rm_loadbalancer:
        resource_group: "{{ resource_group_name }}"
        name: "{{ loadbalancer_name }}"
        sku: Standard
        frontend_ip_configurations:
          - name: frontend-ip-conf
            public_ip_address: "{{ public_ip_name }}"
        probes:
          # - name: prob-http
          #   port: 80
          #   protocol: Http
          #   request_path: /
          #   fail_count: 2
          #   interval: 5
          - name: prob-https
            port: 443
            protocol: Https
            request_path: /api/status
            fail_count: 2
            interval: 5
        backend_address_pools:
          - name: backend-addr-pool
        load_balancing_rules:
          # - name: http-rule
          #   frontend_ip_configuration: frontend-ip-conf
          #   backend_address_pool: backend-addr-pool
          #   frontend_port: 80
          #   backend_port: 80
          #   probe: prob-http
          - name: https-rule
            frontend_ip_configuration: frontend-ip-conf
            backend_address_pool: backend-addr-pool
            frontend_port: 443
            backend_port: 443
            probe: prob-https

    - name: Create virtual machine scale set
      azure_rm_virtualmachinescaleset:
        resource_group: "{{ resource_group_name }}"
        name: "{{ vmss_name }}"
        vm_size: "{{ api_vm_size }}"
        capacity: "{{ vmss_capacity }}"
        overprovision: no
        upgrade_policy: Manual
        managed_disk_type: Standard_LRS
        admin_username: "{{ admin_username }}"
        ssh_password_enabled: false
        ssh_public_keys:
          - path: "/home/{{ admin_username }}/.ssh/authorized_keys"
            key_data: "{{lookup('file', '../../../ssh-keys/toptal.pub')}}"
        virtual_network_name: "{{ virtual_network_name }}"
        subnet_name: "{{ subnet_name }}"
        security_group: "{{ security_group_name }}"
        load_balancer: "{{ loadbalancer_name }}"
        image: "{{ os_image }}"

    - name: Get list of virtual machine scale set network interfaces
      azure_rm_resource_info:
        url: "/subscriptions/{{ subscriptionId }}/resourceGroups/{{ resource_group_name }}/providers/Microsoft.Compute/virtualMachineScaleSets/{{ vmss_name }}/networkInterfaces?2018-10-01"
      register: vmss_nic_output

    - name: Add hosts to api group
      add_host:
        groups: 
          - toptal
          - api
        hostname: "API({{ item }})"
        ansible_ssh_host: "{{ item }}"
        ansible_ssh_user: "{{ admin_username }}"
        ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q {{ admin_username }}@{{ jump_host_ip }}" -o StrictHostKeyChecking=no'
        ansible_python_interpreter: /usr/bin/python3
      loop: "{{ vmss_nic_output | json_query('response[*].properties.ipConfigurations[?name==`default`].properties.privateIPAddress') | flatten }}"
      no_log: true

