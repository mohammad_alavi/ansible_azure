---
- include: setup-RedHat.yml
  when: ansible_os_family == 'RedHat'

- include: setup-Debian.yml
  when: ansible_os_family == 'Debian'

- name: Install Filebeat.
  package: name=filebeat state=present

- name: Install Metricbeat.
  package: name=metricbeat state=present

- name: Copy Filebeat configuration.
  template:
    src: filebeat.yml.j2
    dest: "/etc/filebeat/filebeat.yml"
    owner: root
    group: root
    mode: 0644
  notify:
    - restart filebeat

- name: Copy Metricbeat configuration.
  template:
    src: metricbeat.yml.j2
    dest: "/etc/metricbeat/metricbeat.yml"
    owner: root
    group: root
    mode: 0644
  notify:
    - restart metricbeat

- name: Enable Filebeat modules
  shell: 
    cmd: "filebeat modules enable {{ item }}"
    creates: "/etc/filebeat/modules.d/{{ item }}.yml"
  loop: "{{ filebeat_modules }}"

- name: Enable Metricbeat modules
  shell: 
    cmd: "metricbeat modules enable {{ item }}"
    creates: "/etc/metricbeat/modules.d/{{ item }}.yml"
  loop: "{{ metricbeat_modules }}"

- name: Create Filebeat dashboard directory
  file:
    path: /etc/filebeat/7/dashboard
    recurse: true
    state: directory

- name: Create Metricbeat dashboard directory
  file:
    path: /etc/metricbeat/7/dashboard
    recurse: true
    state: directory

- name: Copy Filebeat dashboard files
  copy:
    src: "{{ item }}"
    dest: "/etc/filebeat/7/dashboard/{{ item }}"
  loop:
    - Filebeat-nginx-overview.json
    - Filebeat-nginx-logs.json
    - Filebeat-Postgresql-overview.json
    - Filebeat-Postgresql-slowlogs.json
    - Filebeat-syslog.json
    - Filebeat-ssh-login-attempts.json
    - Filebeat-auth-sudo-commands.json
    - Filebeat-new-users-and-groups.json
  notify:
    - restart filebeat

- name: Copy Metricbeat dashboard files
  copy:
    src: "{{ item }}"
    dest: "/etc/metricbeat/7/dashboard/{{ item }}"
  loop:
    - Metricbeat-docker-overview.json
    - Metricbeat-host-overview.json
    - Metricbeat-Host-Services-overview.json
    - Metricbeat-nginx-overview.json
    - Metricbeat-postgresql-overview.json
    - Metricbeat-system-overview.json
  notify:
    - restart metricbeat

- name: Create PKI directory
  file:
    path: /etc/filebeat/pki
    state: directory

- name: Copy SSL certificate
  copy:
    src: "{{ item }}"
    dest: "/etc/filebeat/pki/{{ item }}"
    owner: root
    group: root
    mode: '0660'
  loop:
    - ca.crt

- name: Create Filebeat keystore
  shell:
    cmd: filebeat keystore create
    creates: /var/lib/filebeat/filebeat.keystore
  notify:
    - restart filebeat

- name: Create Metricbeat keystore
  shell:
    cmd: metricbeat keystore create
    creates: /var/lib/metricbeat/metricbeat.keystore
  notify:
    - restart metricbeat

- name: Check if Filebeat password is already added to the keystore
  shell:
    cmd: filebeat keystore list
  changed_when: false
  register: filebeat_keystore

- name: Check if Metricbeat password is already added to the keystore
  shell:
    cmd: metricbeat keystore list
  changed_when: false
  register: metricbeat_keystore

- name: Add Filebeat elastic user password to keystore
  shell:
    cmd: echo '"{{ filebeat_elastic_password }}"' | filebeat keystore add --stdin ES_PWD
  notify:
    - restart filebeat    
  when: filebeat_keystore.stdout.find('ES_PWD') == -1

- name: Add Metricbeat elastic user password to keystore
  shell:
    cmd: echo '"{{ metricbeat_elastic_password }}"' | metricbeat keystore add --stdin ES_PWD
  notify:
    - restart metricbeat    
  when: metricbeat_keystore.stdout.find('ES_PWD') == -1

- name: Ensure monitoring-elasticsearch is in hosts file.
  lineinfile:
    dest: /etc/hosts
    regexp: '.*monitoring-elasticsearch$'
    line: "{{ elasticsearch_ip }} monitoring-elasticsearch"
    state: present

- name: Ensure monitoring-kibana is in hosts file.
  lineinfile:
    dest: /etc/hosts
    regexp: '.*monitoring-kibana$'
    line: "{{ kibana_ip }} monitoring-kibana"
    state: present
      
- name: Ensure Filebeat is started and enabled at boot.
  service:
    name: filebeat
    state: started
    enabled: true

- name: Ensure Metricbeat is started and enabled at boot.
  service:
    name: metricbeat
    state: started
    enabled: true
